import React from 'react';
import Example from './Example';
import './App.css';

const App = () => (<Example />);

export default App;
